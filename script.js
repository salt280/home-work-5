"use strict";

// 1. В чому полягає відмінність localStorage і sessionStorage?


// localStorage і sessionStorage - це два об'єкти веб-сховища, які дозволяють 
// зберігати дані на боці клієнта веб-браузера. Основна відмінність між ними 
// полягає в тому, як довго зберігається інформація та як вона доступна.

// localStorage:

// Тривалість зберігання даних: Дані, збережені в localStorage, залишаються на 
// пристрої користувача навіть після закриття вкладки браузера та перезапуску пристрою. 
// Ці дані залишаються на термін визначений користувачем або доки вони явно не будуть видалені.

// Доступність: Дані в localStorage доступні для всіх вкладок та вікон браузера з 
// однієї й тієї ж самої доменної області (тобто для одного домену та його піддоменів).

// sessionStorage:

// Тривалість зберігання даних: Дані, збережені в sessionStorage, залишаються лише на час 
// тривалості сесії браузера. Якщо користувач закриє вкладку або вимкне браузер, дані будуть 
// видалені.

// Доступність: Дані в sessionStorage доступні тільки для тієї вкладки (або вікна), 
// в якій вони були створені. Інші вкладки або вікна не можуть отримати доступ до цих даних.



// 2. Які аспекти безпеки слід враховувати при збереженні чутливої інформації, такої як паролі, 
// за допомогою localStorage чи sessionStorage?


// Зберігання чутливої інформації, такої як паролі, в localStorage або sessionStorage, 
// може стати об'єктом атак, оскільки ці дані доступні на стороні клієнта. Щоб забезпечити 
// безпеку при роботі з такою інформацією, рекомендується дотримуватися кількох принципів:

// Хешування паролів: Замість зберігання паролів в чистому вигляді, слід використовувати 
// алгоритми хешування (наприклад, bcrypt або Argon2) для збереження хеш-значень паролів. 
// Таким чином, навіть якщо дані потраплять в руки зловмисників, важко буде отримати вихідні паролі.

// Використання HTTPS: Забезпечте з'єднання з вашим сервером через протокол HTTPS. 
// Це допоможе уникнути атак посередника, коли зловмисники намагаються перехопити дані 
// між клієнтом і сервером.

// Використання криптографічних бібліотек: Використовуйте надійні криптографічні бібліотеки 
// для роботи з чутливою інформацією, яка може бути збережена локально. Наприклад, бібліотеки 
// для шифрування і розшифрування даних.

// Обмеження доступу до інформації: Зберігайте на локальному пристрої лише ту інформацію, 
// яка дійсно необхідна для роботи клієнтського застосунку. Не зберігайте чутливі дані, які 
// не використовуються на клієнтському рівні.

// Зберігання токенів замість паролів: Якщо можливо, використовуйте токени доступу замість 
// зберігання паролів. Токени можна видалити або оновити, що зробить їх менш схильними до атак.

// Регулярне оновлення та перевірка безпеки: Періодично переглядайте та оновлюйте свої 
// безпекові заходи, слідкуйте за оновленнями бібліотек та забезпечуйте безпеку додатка в цілому.

// Незважаючи на ці заходи, рекомендується уникати зберігання чутливої інформації на 
// клієнтському боці, якщо це не є необхідністю, і використовувати для цього серверні рішення.



// 3. Що відбувається з даними, збереженими в sessionStorage, коли завершується сеанс браузера?


// sessionStorage - це механізм для зберігання даних в браузері, які будуть доступні лише під 
// час поточного сеансу (поки вікно або вкладка браузера відкрита). Коли сеанс браузера 
// завершується, дані, збережені в sessionStorage, автоматично видаляються, і вони стають 
// недоступними для подальших запитів.

// Отже, основна особливість sessionStorage - це те, що дані в ньому не зберігаються після 
// закриття вкладки або вікна браузера. Це корисно для тимчасового зберігання даних, які повинні 
// бути доступні лише протягом конкретного сеансу роботи з сайтом.




// Практичне завдання:


// Реалізувати можливість зміни колірної теми користувача.



// Технічні вимоги:


// - Взяти готове домашнє завдання HW-5 "Book Shop" з блоку Basic HMTL/CSS.

// - Додати на макеті кнопку "Змінити тему".

// - При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо) на ваш розсуд. При повторному натискання - повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.

// - Вибрана тема повинна зберігатися після перезавантаження сторінки.



// Примітки: 

// - при виконанні завдання перебирати та задавати стилі всім елементам за допомогою js буде вважатись помилкою;

// - зміна інших стилів сторінки, окрім кольорів буде вважатись помилкою.




document.addEventListener('DOMContentLoaded', function() {
    const sectionContent = document.querySelector(".nav-icon-menu");
    const button = document.createElement("button");
    button.id = "btn-input-create";
    button.style.width = "16px";
    button.style.height = "16px";
    button.style.backgroundColor = "blue";
    sectionContent.appendChild(button);

    const backgroundColor = document.querySelector(".body-container");
    const storedColor = localStorage.getItem('backgroundColor');
    const backgroundColorHeader = document.querySelector(".nav-bar");
    const storedColorHeader = localStorage.getItem('backgroundColorHeader');
    const storedButtonColor = localStorage.getItem('buttonColor');

    if (storedColor && storedColorHeader) {
        backgroundColor.style.backgroundColor = storedColor;
        backgroundColorHeader.style.backgroundColor = storedColorHeader;
    }

    if (storedButtonColor) {
        button.style.backgroundColor = storedButtonColor;
    }

    button.addEventListener("click", () => {
        if (
            backgroundColor.style.backgroundColor === "blue" &&
            backgroundColorHeader.style.backgroundColor === "blue" &&
            button.style.backgroundColor === "yellow"
        ) {
            backgroundColor.style.backgroundColor = "yellow";
            backgroundColorHeader.style.backgroundColor = "yellow";
            button.style.backgroundColor = "blue";
        } else {
            backgroundColor.style.backgroundColor = "blue";
            backgroundColorHeader.style.backgroundColor = "blue";
            button.style.backgroundColor = "yellow";
        }

        localStorage.setItem('backgroundColor', backgroundColor.style.backgroundColor);
        localStorage.setItem('backgroundColorHeader', backgroundColorHeader.style.backgroundColor);
        localStorage.setItem('buttonColor', button.style.backgroundColor);
    });
});
